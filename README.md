# package packedstruct
## Requirements
* x64 (amd64)
* Go
* FreeBSD, Linux

## Goals
This project is still heavily Work In Progress.
Some projects I'm working on are using C libraries to send and receive messages over networks, some of them are also sending C packed structs of data. With CGO, I know it was possible to have some Go packages or programs to use C, or even to have C libraries or programs to use Go. It is quite easy to take some C structures and to get their data in Go structures. But once you're packing those C structures to get less memory usage, it's still possible to get the C data in Go, but it's hard to decode this data.

It seems to me that this is more like decoding data from C to Go, this is why this package is working more like `encoding/json` package.
It may not be perfect, but it's really hard to get more with Go/CGO from these C packed structures.

## Note
These C projects are working very well, and by creating this package I do not intend to replace any of them. I'm mostly trying to extend these usages with a language like Go. While quite interesting, it's also nice translating C to Go and to learn good and best practices.
