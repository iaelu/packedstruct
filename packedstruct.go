package packedstruct

import (
	"bytes"
	"encoding/binary"
	"reflect"
	"strconv"
)

// Decoder is the helper to decode from C packed struct
type Decoder struct {
	data []byte
}

// NewDecoder returns a new decoder that reads from data buffer
func NewDecoder(data []byte) *Decoder {
	return &Decoder{
		data: data,
	}
}

func getRightMask(size int) uint64 {
	mask := uint64(0)
	for i := 0; i < size; i++ {
		mask |= 1 << i
	}
	return mask
}

// NilValueArgument represents nil value argument error
type NilValueArgument struct{}

func (e *NilValueArgument) Error() string {
	return "value argument is nil"
}

// Decode reads a C packed struct from its input and stores it in the value pointed to by v
func (d *Decoder) Decode(v interface{}) error {
	if v == nil {
		return &NilValueArgument{}
	}
	buffer := bytes.NewBuffer(d.data)
	typ := reflect.TypeOf(v).Elem()
	val := reflect.ValueOf(v).Elem()

	lshift := 0
	sumshift := 0
	leftval := uint64(0)
	i := 0
	for i < typ.NumField() && buffer.Len() != 0 {
		field := val.Field(i)
		ftype := typ.Field(i)
		bits := int(ftype.Type.Size() * 8)
		fmask := getRightMask(bits)

		switch kind := ftype.Type.Kind(); kind {
		case reflect.Ptr, reflect.Uintptr, reflect.Slice:
			// just read the pointer so we move to buffer
			uival := uint64(0)
			binary.Read(buffer, binary.LittleEndian, &uival)

		case reflect.Array:
			switch ekind := ftype.Type.Elem().Kind(); ekind {
			case reflect.Ptr, reflect.Uintptr, reflect.Slice:
				// just read the pointer so we move to buffer
				uival := uint64(0)
				for a := 0; a < ftype.Type.Len(); a++ {
					binary.Read(buffer, binary.LittleEndian, &uival)
				}

			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32,
				reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16,
				reflect.Uint32, reflect.Uint64:
				fval := reflect.New(ftype.Type).Elem()
				pfval := fval.Addr().Interface()
				binary.Read(buffer, binary.LittleEndian, pfval)
				pval := reflect.ValueOf(pfval).Elem()
				field.Set(pval)
			}

		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32,
			reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16,
			reflect.Uint32, reflect.Uint64:
			fshift := 0
			if sshift := ftype.Tag.Get("shift"); sshift != "" {
				var err error
				fshift, err = strconv.Atoi(ftype.Tag.Get("shift"))
				if err != nil {
					return err
				}
			}
			if fshift == 0 {
				fshift = bits
			}
			fdummy := false
			if sdummy := ftype.Tag.Get("dummy"); sdummy != "" {
				var err error
				fdummy, err = strconv.ParseBool(ftype.Tag.Get("dummy"))
				if err != nil {
					return err
				}
			}
			if (sumshift + fshift) > (sumshift + lshift) {
				var pval reflect.Value
				var uival uint64

				rmask := getRightMask(fshift - lshift)
				lmask := uint64(^rmask) & fmask
				fval := reflect.New(ftype.Type).Elem()
				pfval := fval.Addr().Interface()
				if lshift == 0 {
					binary.Read(buffer, binary.LittleEndian, pfval)
					pval = reflect.ValueOf(pfval).Elem()
					uival = pval.Uint() & rmask
				} else {
					fsize := ftype.Type.Size()
					temp := make([]byte, fsize)
					tsize := fsize
					if tsize > 1 {
						tsize--
					}
					bits = int(tsize * 8)
					tempval := reflect.ValueOf(temp[:tsize:tsize])
					ptempval := tempval.Interface()
					binary.Read(buffer, binary.LittleEndian, ptempval)
					tempbuf := bytes.NewBuffer(temp[:fsize:fsize])
					binary.Read(tempbuf, binary.LittleEndian, pfval)
					pval = reflect.ValueOf(pfval).Elem()
					if (fshift - lshift) <= bits {
						uival = ((pval.Uint() & rmask) << lshift) | leftval
					} else {
						leftval |= pval.Uint() << lshift
						lshift += bits
						sumshift = fshift - lshift
						continue
					}
				}
				if !fdummy {
					field.Set(reflect.ValueOf(uival).Convert(ftype.Type))
				}
				if fshift-lshift < bits {
					leftval = (pval.Uint() & lmask) >> (fshift - lshift)
					sumshift = fshift - lshift
					lshift = bits - (fshift - lshift)
				} else {
					leftval = 0
					sumshift = 0
					lshift = 0
				}
			} else {
				if fshift > lshift {
					continue
				}
				rmask := getRightMask(fshift)
				lmask := uint64(^rmask) & fmask
				uival := leftval & rmask
				if !fdummy {
					field.Set(reflect.ValueOf(uival).Convert(ftype.Type))
				}
				if fshift < lshift {
					leftval = (leftval & lmask) >> fshift
					sumshift += fshift
					lshift -= fshift
				} else {
					lshift = 0
					sumshift = 0
					leftval = 0
				}
			}
		}
		i++
	}
	return nil
}
