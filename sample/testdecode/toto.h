#ifndef __TOTO_H__
#define __TOTO_H__

typedef struct _toto toto;
typedef struct _tata tata;
typedef struct _tutu tutu;

struct _toto {
  unsigned idx : 7;
  unsigned format : 4;
  unsigned datasz : 10;
  unsigned dummy : 3;
  char *data;
  char name[10];
} __attribute__((packed));

struct _tata {
  unsigned idx : 5;
  unsigned format : 2;
  unsigned update : 2;
  unsigned deleted : 2;
  unsigned datasz : 10;
  unsigned dummy : 3;
  char *data;
} __attribute__((packed));

struct _tutu {
  unsigned idx : 5;
  unsigned format : 3;
  unsigned update : 2;
  unsigned deleted : 2;
  unsigned datasz : 10;
  unsigned dummy : 2;
  char *data;
} __attribute__((packed));

#endif /* __TOTO_H__ */
