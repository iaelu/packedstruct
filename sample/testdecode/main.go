package main

/*
#cgo CFLAGS: -I.
#include <string.h>
#include <stdlib.h>
#include "toto.h"

toto *getToto(void) {
	toto *t;
	t = calloc(1, sizeof(toto));
	if (t == NULL) {
		return(NULL);
	}
	t->idx = 66;
	t->format = 11;
	strcpy(t->name, "toto");
	t->datasz = 11;
	return(t);
}
tata *getTata(void) {
	tata *t;
	t = calloc(1, sizeof(tata));
	if (t == NULL) {
		return(NULL);
	}
	t->idx = 21;
	t->format = 3;
	t->update = 1;
	t->deleted = 2;
	t->datasz = 12;
	return(t);
}
tutu *getTutu(void) {
	tutu *t;
	t = calloc(1, sizeof(tutu));
	if (t == NULL) {
		return(NULL);
	}
	t->idx = 21;
	t->format = 2;
	t->update = 3;
	t->deleted = 1;
	t->datasz = 13;
	return(t);
}
*/
import "C"
import (
	"fmt"
	"os"
	"unsafe"

	"gitlab.com/iaelu/packedstruct"
)

// Toto fait du velo
type Toto struct {
	Idx      uint8  `shift:"7"`
	Format   uint8  `shift:"4"`
	Datasz   uint16 `shift:"10"`
	Dummy    byte   `shift:"3" dummy:"true"`
	Data     []Tata
	NameData [10]byte
}

// Tata fait du chocolat
type Tata struct {
	Idx     uint8  `shift:"5"`
	Format  uint8  `shift:"2"`
	Update  uint8  `shift:"2"`
	Deleted uint8  `shift:"2"`
	Datasz  uint16 `shift:"10"`
	Dummy   byte   `shift:"3" dummy:"true"`
	Data    []byte
}

// Tutu fait du tapecul
type Tutu struct {
	Idx     uint8  `shift:"5"`
	Format  uint8  `shift:"3"`
	Update  uint8  `shift:"2"`
	Deleted uint8  `shift:"2"`
	Datasz  uint16 `shift:"10"`
	Dummy   byte   `shift:"2" dummy:"true"`
	Data    []byte
}

func main() {
	do := C.getToto()
	fmt.Printf("%+v %+v\n", C.sizeof_toto, do)
	dto := packedstruct.NewDecoder(C.GoBytes(unsafe.Pointer(do), C.sizeof_toto))
	toto := Toto{}
	if err := dto.Decode(&toto); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	C.free(unsafe.Pointer(do))
	fmt.Printf("%+v\n", toto)
	size := len(toto.NameData)
	name := string(toto.NameData[:size])
	fmt.Printf("[%s]\n", name)
	fmt.Println()

	da := C.getTata()
	fmt.Printf("%+v %+v\n", C.sizeof_tata, da)
	dta := packedstruct.NewDecoder(C.GoBytes(unsafe.Pointer(da), C.sizeof_tata))
	tata := Tata{}
	if err := dta.Decode(&tata); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	C.free(unsafe.Pointer(da))
	fmt.Printf("%+v\n", tata)
	fmt.Println()

	du := C.getTutu()
	fmt.Printf("%+v %+v\n", C.sizeof_tutu, du)
	dtu := packedstruct.NewDecoder(C.GoBytes(unsafe.Pointer(du), C.sizeof_tutu))
	tutu := Tutu{}
	if err := dtu.Decode(&tutu); err != nil {
		fmt.Println(err)
		os.Exit(3)
	}
	C.free(unsafe.Pointer(du))
	fmt.Printf("%+v\n", tutu)
	fmt.Println()
}
